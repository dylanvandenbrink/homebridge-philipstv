const PhilipsAPI = require('./philipstvapi.js');

var Service, Characteristic;

module.exports = function(homebridge) {
    Service = homebridge.hap.Service;
    Characteristic = homebridge.hap.Characteristic;
    
    homebridge.registerAccessory("homebridge-philipstv-nojointspace", "philipstv-nojointspace", philipsTV);
};

function philipsTV(log, config) {
    this.log = log;
    this.host = config['host'];

    if (!this.host) {
        this.log('Config is missing host/IP of TV');
        callback(new Error('No host/IP defined.'));
        return;
    }
    this.log('PhilipsTV initialized with host: ' + this.host);

    this.api = new PhilipsAPI(this.host, this.log);
}

philipsTV.prototype = {
    getBrightness: function(callback) {
        var log = this.log;
        this.api.getVolume(function(response) {
            if (response && response.current) {
                var volume = response.current;
                log('Got TV volume: ' + volume);
                callback(null, volume);
            } else {
                log('Unable to get TV volume: ' + JSON.stringify(response));
                callback(null);
            }
        });
    },

    setBrightness: function(newLevel, callback) {
        var log = this.log;
        this.api.setVolume(newLevel, function(response) {
            if (response && response.success) {
                log('TV volume was set to: ' + response.relativeVolume);
                callback(null);
            } else {
                log('Unable to set TV volume: ' + JSON.stringify(response));
            }
        });
    },

    getPowerOn: function(callback) {
        var log = this.log;
        this.api.getInfo(function(info) {
            if (info && info.name) {
                log('TV is on');
                callback(null, 1);
            } else {
                log('TV is off');
                callback(null, 0);
            }
        });
    },

    setPowerOn: function(powerOn, callback) {
        var log = this.log;
        if (powerOn) {
            log('Turning on is not supported');
            callback(null);
        } else {
            log('Turning off tv...');
            this.api.turnOff(function(result) {
                if (result && result.success) {
                    log('TV was turned off')
                    callback(null);
                } else {
                    log('Error turning off TV: ' + JSON.stringify(result));
                    callback(null);
                }
            });
        }
    },

    getServices: function() {
        let informationService = new Service.AccessoryInformation();
        informationService
            .setCharacteristic(Characteristic.Manufacturer, "Dylan van den Brink")
            .setCharacteristic(Characteristic.Model, "Philips 43PUS6162/12")
            .setCharacteristic(Characteristic.SerialNumber, "1.0.19");
    
        let lightbulbService = new Service.Lightbulb('philipsTV');
        lightbulbService
            .getCharacteristic(Characteristic.On)
                .on('get', this.getPowerOn.bind(this))
                .on('set', this.setPowerOn.bind(this));

        lightbulbService
            .addCharacteristic(new Characteristic.Brightness())
                .on('get', this.getBrightness.bind(this))
                .on('set', this.setBrightness.bind(this));
    
        this.informationService = informationService;
        this.lightbulbService = lightbulbService;

        return [informationService, lightbulbService];
    }
};
