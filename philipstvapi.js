var request = require('request');

function PhilipsAPI(host) {
    if (!(this instanceof PhilipsAPI)) {
        return new PhilipsAPI(host);
    }
  
    this.host = host;
};

PhilipsAPI.prototype.getInfo = function(callback) {
    let url = `http://${this.host}:1925/1/system`;
    request.get(url, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            callback(JSON.parse(body));
        } else {
            callback(null);
        }
    });
}

PhilipsAPI.prototype.getVolume = function(callback) {
    let url = `http://${this.host}:1925/6/audio/volume`;
    request.get(url, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            callback(JSON.parse(body));
        } else {
            callback(null);
        }
    });
}

PhilipsAPI.prototype.setVolume = function(volume, callback) {
    let url = `http://${this.host}:1925/6/audio/volume`;
    let relativeVolume = ((60/100) * volume).toFixed(0);
    request({
        uri: url,
        method: 'POST',
        json: {
            muted: false,
            current: volume
        }
    }, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            callback({ success: true, relativeVolume: relativeVolume});
        } else {
            callback(error);
        }
    });
}
  
PhilipsAPI.prototype.turnOff = function turnOff(callback) {
    let url = `http://${this.host}:1925/6/input/key`;
    request({
        uri: url,
        method: 'POST',
        json: {
            key: 'PowerOff'
        }
    }, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            callback({ success: true });
        } else {
            callback(error);
        }
    });
};
  
module.exports = PhilipsAPI;