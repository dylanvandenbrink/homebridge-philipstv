# homebridge-philipstv-nojointspace

A homebridge-plugin to control the following Philips TV models:
* 43PUS6162/12 (2017)

## Configuration

```json
"accessories": [
    {
        "accessory": "philipstv-nojointspace",
        "name": "TV",
        "host": "192.168.2.114"
    }
]
```